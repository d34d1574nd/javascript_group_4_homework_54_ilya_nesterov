import React, { Component } from 'react';
import Card from './components/Card/Card';
import CardDeck from './libs/CardDeck';
import PokerHand from "./libs/PokerHand";
import './App.css';

class App extends Component {
  state = {
    cards: []
  };

  addCard = () => {
      const cards = new CardDeck().getCards(5);
      this.setState({cards});
  };

  pokerHand = () => {
    return new PokerHand(this.state.cards).getOutcome();
  };

  render() {
    return (
      <div className="App">
        <p>{this.pokerHand()}</p>
        <Card
          addhandler = {this.addCard}
          cards = {this.state.cards}
          hands={this.pokerHand}
        />
      </div>
    );
  }
}

export default App;
